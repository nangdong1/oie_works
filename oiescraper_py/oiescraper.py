# coding = UTF-8

print('로딩 중입니다.\n')

import os, time, requests, re, sqlite3, pandas as pd, warnings, urllib.request
from bs4 import BeautifulSoup
from tqdm import tqdm  # 진행표시줄
from oiescraper_sub import get_oie_report
from oiescraper_kor import translate_oie_reports
from oiescraper_totalhead import totalhead

def get_oie_data():

    '''각 report의 링크를 목록화'''
    url = 'https://www.oie.int/wahis_2/public/wahid.php/Diseaseinformation/WI/index/newlang/en'  # oie report 리스트의 url

    response = requests.get(url, verify=False)
    soup = BeautifulSoup(response.content, 'lxml')  # BeautifulSoup을 이용해 html 가져옴
    # with urllib.request.urlopen(url) as response:  
        # soup = BeautifulSoup(response.read(), 'lxml')

    tags = soup.select('table a')  # 원하는 자료가 table 태그 아래 a 태그에 속해 있음
    hrefs = [tag['href'] for tag in tags]  # 속성 href의 값 추출
    
    # url은 이미 알고 있기 때문에 굳이 link 뽑을 필요 없이 유일하게 변하는 부분인 링크번호만 뽑음
    linknumbers = [re.search('[0-9]{3,7}', href).group() for href in hrefs]  # [0-9]: 숫자 문자열. {3,7}: 앞 문자가 3~7개 있어야 함

    
    '''기존 db의 링크번호와 비교하여 새로운 링크번호만 추출'''
    
    # weekly reports 상단 150개의 링크번호
    number = 150
    want_linknumbers = [linknumbers[n] for n in reversed(range(number))]  

    # db에 연결
    con = sqlite3.connect('oie_reports.db')

    # 기존 db의 링크번호 가져옴
    linknumber_db_df = pd.read_sql("SELECT [링크 번호] FROM oie_reports_kr", con)  # []: 칼럼명이나 테이블명에 공백이 있을 때
    linknumber_db_df.drop_duplicates(inplace=True)

    linknumber_db_series = linknumber_db_df['링크 번호'].astype(str)  # 숫자형식을 문자형식으로 바꿈

    # 기존 링크번호 목록에 없다면 새로운 링크넘버 리스트에 추가
    new_linknumbers = []

    for want_linknumber in want_linknumbers:
        
        if not (want_linknumber in list(linknumber_db_series)):
            new_linknumbers.append(want_linknumber)
    

    '''새로 올라온 리포트의 데이터 추출'''

    print('데이터를 추출하고 있습니다.\n잠시만 기다려주세요.\n')

    new_error_linknumbers = []  # 에러 링크번호를 추가하기 위함
    new_nobreak_linknumbers = []  # 발생 건 없거나 수생 동물인 링크번호를 추가하기 위함
    oie_reports = pd.DataFrame()  # 리포트의 빈 df. 추출한 리포트 데이터를 추가하기 위함

    for new_linknumber in tqdm(new_linknumbers):  # tqdm: 반복문에서의 리스트 원소 수를 백분율로 나타내어 진행표시줄을 만듦

        # 서버 오류로 인해 timeout 에러가 났을 때 회피
        try:
            oie_report, new_error_linknumber, new_nobreak_linknumber = get_oie_report(new_linknumber)
        
        except TimeoutError:
            new_error_linknumbers.append(new_linknumber)
            break

        # 추출한 데이터 및 링크번호 추가
        oie_reports = oie_reports.append(oie_report, ignore_index=True)  # df.append 사용 시 무조건 ignore_index=True!
        new_error_linknumbers.append(new_error_linknumber)
        new_nobreak_linknumbers.append(new_nobreak_linknumber)

        time.sleep(3)

    # 에러 링크번호 df와 발생 건 없거나 수생 동물 링크번호 df
    new_error_linknumber_df = pd.DataFrame({'error_linknumbers': new_error_linknumbers}).dropna()
    new_nobreak_linknumber_df = pd.DataFrame({'nobreak_linknumbers': new_nobreak_linknumbers}).dropna()


    '''
    추출한 데이터가 하나도 없을 때 넘어가는 코드 
        - oie_reports의 데이터가 하나도 없으면 print() 
        - 있으면 한글화 후 db에 저장
    '''
    
    if oie_reports.empty == True:
        
        trans_dfs = None
        print('\n발생 건이 없습니다.\n')
    
    else:
        # 한글화 - oiescraper_kor.py에서 translate_oie_reports 함수 불러옴
        trans_dfs = translate_oie_reports(oie_reports)
        
        # db 저장
        warnings.filterwarnings(action='ignore')  # 경고 메시지 안 띄우게 함. action='ignore': 무시 'default': 경고 발생 위치에 출력

        trans_dfs[0].to_sql('oie_reports', con, if_exists='append', index=False)
        trans_dfs[1].to_sql('oie_reports_kr', con, if_exists='append', index=False)

        # 총 두수 df
        totalhead_df = totalhead(trans_dfs[1])

        # 바탕화면에 엑셀파일 추출
        desktop_path = f'C:\\Users\\{os.getlogin()}\\Desktop\\'

        with pd.ExcelWriter(desktop_path + 'new_oie_reports.xlsx') as writer:
            trans_dfs[0].to_excel(writer, '원본', index=False)
            trans_dfs[1].to_excel(writer, '번역본', index=False)
            totalhead_df.to_excel(writer, '해동(총 두수)')
        
        print('\n완료되었습니다.\n만들어진 엑셀 파일을 확인하세요.\n')  # 개수 입력 후 콘솔에 보여질 문구
    
    con.close()
    
    os.system('Pause')  # "프로그램 종료하려면 아무 키나 누르세요" 실행
    
    return trans_dfs, totalhead_df, new_error_linknumber_df, new_nobreak_linknumber_df, new_linknumber
    # new_linknumber: 마지막 링크 번호

if __name__ == "__main__":

    data_tuple = get_oie_data()
    
    trans_dfs = data_tuple[0]
    totalhead_df = data_tuple[1]

    new_error_linknumber_df, new_nobreak_linknumber_df, last_linknumber = data_tuple[2:]
    
    oie_nums = pd.DataFrame(trans_dfs[0]['Link number'].drop_duplicates().dropna())  # 추출한 데이터의 링크 번호 df
    total_nums = pd.concat([oie_nums, new_error_linknumber_df, new_nobreak_linknumber_df], ignore_index=True, axis=0)  
    # 총 링크번호 df. 위 df에 에러 링크번호 df와 발생 건 없음 링크번호 df 결합
# coding = UTF-8

import re, pandas as pd

def get_oie_report(linknumber):

    # 반환값을 미리 None으로 지정함
    report_df, error_linknumber, nobreak_linknumber = (None, None, None)

    # format(f) 함수를 이용해 new_linknumber를 넣을 url를 만듦
    url = f'https://www.oie.int/wahis_2/public/wahid.php/Reviewreport/Review?page_refer=MapFullEventReport&reportid={linknumber}'

    
    '''table 태그 추출. 문제 발생 시 error_linknumber로 저장하고 return'''
    
    try:
        table = pd.read_html(url, flavor='lxml')  # 각각의 table 태그를 df로 변환 후 이 모두를 하나의 리스트로 묶음(table = [table[0], table[1], ..])
    
    except:
        error_linknumber = linknumber

        return report_df, error_linknumber, nobreak_linknumber


    '''발생 건 없을 때 or 수생 동물 건일 때 nobreak_linknumber로 저장하고 return'''

    table_text_df = table[0]  # table_text_df: 각 table 태그의 텍스트를 행에 하나씩 대응하여 만든 df
    
    if (not any(table_text_df[0].str.contains('Total outbreaks'))) or any(table_text_df[0].str.contains('Water type')):
        nobreak_linknumber = linknumber

        return report_df, error_linknumber, nobreak_linknumber


    '''Summary 박스'''
    
    summary_df = table[3].T  # T: 행 열 전환
    
    # summary df의 첫 행을 칼럼명으로 지정
    summary_header = summary_df.loc[0]  
    summary_df = summary_df.loc[1:].rename(columns=summary_header).reset_index(drop=True)  
    # reset_index: 인덱스 리셋. drop: True - 기존 인덱스열을 없앨지(True) 새로운 칼럼으로 만들지(False)


    '''
    발생 건 박스
        -  table 리스트에서 발생 건 df에 해당하는 범위를 찾아 하나의 df로 만듦
    '''
    
    # total_index: 텍스트 df에서 Total outbreaks 문자열 들어간 행 위치 - index.to_list()[0]: 인덱스 type을 리스트로 변환하여 숫자 추출
    total_index = table_text_df[table_text_df[0].str.contains('Total outbreaks')].index.to_list()[0]
    total_table_index = (total_index - 4) * 2 + 4  # table 리스트에서 total 박스 df의 위치
    
    outbreaks_df = pd.DataFrame()  # 먼저 발생 건 df를 빈 df로 만들어 놓음. 새 df를 추가할 예정
    
    for n in range(4, total_table_index, 2):  # 발생 건 박스 범위(table[4]에서 total 박스 앞까지)

        # 발생 건 수
        '''
        cluster이면 발생 지역의 괄호 부분에 명시된 outbreaks의 숫자를 추출해 '발생 건 수'열에 대입
        없으면 '1'을 대입
        '''
        if re.search('cluster', table[n].loc[0, 0]):  # 정규식 사용 - group(1): 첫 번째 그룹인 ([0-9]+) 
            cluster_outbreaks_number = re.search('([0-9]+)\s*outbreaks', table[n].loc[0, 1]).group(1)
            table[n].loc[len(table[n])] = ['Number of outbreaks', cluster_outbreaks_number]  # len(df) : df행 길이
        
        else:
            table[n].loc[len(table[n])] = ['Number of outbreaks', '1']
        
        # 'Outbreak 1'부분을 'Region'으로 바꿈
        table[n].loc[0, 0] = 'Region'

        outbreak_df = pd.concat([table[n].T, table[n + 1]], axis=1, ignore_index=True)  # 발생 건 박스 1개에 대한 df를 만듦
        
        # outbreak df의 첫 행을 칼럼명으로 지정
        outbreak_header = outbreak_df.loc[0]  
        outbreak_df = outbreak_df.loc[1:].rename(columns=outbreak_header).reset_index(drop=True)
    
        outbreaks_df = pd.concat([outbreaks_df, outbreak_df], ignore_index=True)  # 빈 df에 위 df를 추가. 반복문 실행 후 모두 추가하면 해당 리포트의 발생 건 df 완성


    '''summary df와 발생 건 df 결합'''

    summary_outbreaks_df = pd.concat([summary_df, outbreaks_df], axis=1)
    
    # '발생 건 수' 열과 '두 수' 열을 제외한 열의 빈 셀을 위 행 값으로 채우기
    summary_outbreaks_df.iloc[:, :-7] = summary_outbreaks_df.iloc[:, :-7].fillna(method='ffill')  # 슬라이싱한 df는 inplace=True가 안 되는 듯

    # '국가', '질병', '링크 번호'열 삽입
    country_name = table_text_df.loc[0, 0].split(',')[-1]
    disease = table_text_df.loc[0, 0].split(',')[0]
    
    summary_outbreaks_df['Country'] = country_name
    summary_outbreaks_df['Disease'] = disease
    summary_outbreaks_df['Link number'] = linknumber


    '''원하는 데이터 열만 가져오기'''

    want_header = ['Report type', 'Affected population', 'Causal agent', 'Link number', 'Disease', 'Country', 'Serotype',
                   'Report date', 'Number of outbreaks', 'Date of start of the outbreak', 'Region', 'Epidemiological unit',
                   'Species', 'Susceptible', 'Cases', 'Deaths', 'Killed and disposed of', 'Slaughtered']

    '''
    'Serotype' or 'Affected population' or 'Causal agent' 열이 없는 리포트도 있음
        - 위에서 결합한 df에 위의 칼럼명(want_header)이 있는지 확인하는 작업
        - 없다면 해당 칼럼명의 빈 셀로 된 열을 삽입    
    '''
    for header in want_header:
        if not header in list(summary_outbreaks_df.columns):
            summary_outbreaks_df[header] = float('nan')

    summary_outbreaks_df = summary_outbreaks_df[want_header]

    
    '''
    조치사항 박스
        - 이를 위 summary_발생 건 df와 결합하여 최종 df 완성
    '''

    # 조치사항 전체 목록
    measure_header = ['Movement control inside the country', 'Vaccination in response to the outbreak (s)',
                      'Surveillance outside containment and/or protection zone',
                      'Surveillance within containment and/or protection zone', 'Screening', 'Traceability',
                      'Quarantine', 'Official destruction of animal products',
                      'Official disposal of carcasses, by-products and waste',
                      'Process to inactivate the pathogenic agent in products or by-products', 'Stamping out',
                      'Selective killing and disposal', 'Control of wildlife reservoirs', 'Zoning',
                      'Disinfection', 'Disinfestation', 'Control of vectors', 'Vector surveillance',
                      'Ante and post-mortem inspections',
                      'Vaccination permitted (if a vaccine exists)', 'Vaccination prohibited',
                      'No treatment of affected animals', 'Treatment of affected animals', 'Slaughter']

    # 위 칼럼명으로 된 빈 df 만들어 놓음
    measure_df = pd.DataFrame(columns=measure_header)

    # 빈 조치사항 df와 summary_발생 건 df를 미리 결합한 후 값을 추가
    report_df = pd.concat([summary_outbreaks_df, measure_df], axis=1)  

    '''
    텍스트 df에서 조치사항 박스의 제목인 'Control measures'로 시작하는 행 위치(measure_index)와 그 값(measure_text)
    measure_text는 하나의 문자열로 되어 있음(예: 'Control of vectors  Vaccination prohibited  Measures to be applied  No other measures')
    '''
    measure_index = table_text_df[table_text_df[0].str.startswith('Control measures')].index.to_list()[0]  # startswith: 해당 문자열로 시작
    measure_text = re.search('applied\s*(.*)', table_text_df.loc[measure_index, 0]).group(1)  # 해당 리포트의 조치사항 목록. 'applied '의 뒷 부분

    '''
    measure_text에 조치할 사항 문구('Measures to be applied')가 들어 있다면
        - 'Measures to be applied' 문구의 앞부분은 조치한 사항, 뒷부분은 조치할 사항으로 나눔
        - 안 들어 있으면 measure_text 모두 조치한 사항
    '''
    if 'Measures to be applied' in measure_text:
        measure_applied = re.search('(.*)\s*Measures to be applied\s*(.*)', measure_text).group(1)
        measure_tobe = re.search('(.*)\s*Measures to be applied\s*(.*)', measure_text).group(2)
    
    else:
        measure_applied = measure_text
        measure_tobe = 'No other measures'

    '''
    measure_header의 원소가 measure_applied에 들어 있으면 'T', measure_tobe에 들어 있으면 'To be'
        - 'Treatment of affected animals (To prevent secondary bacterial infection)'나 
            'Disinfection / Disinfestation'과 같은 변종도 잡을 수 있음
    '''
    for measure in measure_header:
        
        if measure in measure_applied:
            report_df[measure] = 'Y'
        
        elif not ('No other measures' in measure_tobe):  # 조치할 사항에 'No other measures'가 있으면 스킵
            
            if measure in measure_tobe:
                report_df[measure] = 'To be'

    '''
    열 위치 수정. 선택한 열을 마지막으로 보냄
        - 위 want_header에서 미리 뒤로 보내지 않은 이유: measure_df를 붙인 뒤에 맨 뒤로 보내야 하기 때문
    '''
    report_df_header = list(report_df.columns)
    report_df_header = report_df_header[3:] + report_df_header[:3]
    report_df = report_df[report_df_header]

    return report_df, error_linknumber, nobreak_linknumber


if __name__ == "__main__":
    print('로딩 중입니다.\n')
    # linknumber = '31685'  # 조치사항에 표
    # linknumber = '29044'  # 조치사항 tobe
    # linknumber = '33576'  # 지역명에 원인체명 있음. 사육 두수 빈칸
    # linknumber = '34184'  # 총 두수에서 감염 컬럼에 *표가 있는 것
    # linknumber = '33640'  # 세부 축종 - Affected Population
    # linknumber = '33651'  # 일반. 세부 축종을 넣어보자
    # linknumber = '33695'  # 조치사항 '감염동물치료괄호'
    # linknumber = '29010'  # 조치사항 '소독/해충구제'
    linknumber = '32279'  # 발생 건 박스에 여러 축종
    # linknumber = '34419'  # cluster
    # linknumber = '3060'  # 페이지 오류
    
    # data_tuple = get_oie_report(linknumber)
    # report_df, error_linknumber, nobreak_linknumber = data_tuple

    print('\n완료!')
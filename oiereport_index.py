# coding = UTF-8
print('로딩 중입니다.\n')
import requests, re
from bs4 import BeautifulSoup

url = 'http://www.oie.int/wahis_2/public/wahid.php/Diseaseinformation/WI/index/newlang/en' # oie report 리스트의 url
response = requests.get(url, verify=False)  # HTTP 요청
soup = BeautifulSoup(response.content, 'lxml')  # BeautifulSoup을 이용해 html 분석
tags = soup.select('table a')  # 원하는 자료가 table 태그의 a 태그에 속해 있음
hrefs = [tag['href'] for tag in tags]  # 속성 href의 값 추출. href는 링크를 뜻함
linknumber_list = [re.search('[0-9]{5}', href).group() for href in hrefs]  # $를 뺀 이유 : 5개 숫자 뒤 괄호 때문
# url은 이미 알고 있기 때문에 굳이 link 뽑을 필요 없이 유일하게 변하는 부분인 링크번호만 뽑음

# 해당 링크번호의 인덱스 반환
while True:
    input_linknumber = input('링크 넘버 입력(끝내기는 exit 입력)')  # 해당 링크번호 입력
    if input_linknumber == 'exit':
        print('끝')
        break
    try:
        print(linknumber_list.index(input_linknumber))  # 해당 링크번호의 위치
    except ValueError:
        print('\n해당 링크번호는 목록에 없습니다.\n')
        continue
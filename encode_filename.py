# coding = UTF-8

'''
파일명 암호화
모든 문자를 특수기호로 변환
1. 해당 파일명 입력
	1-1. 경로를 입력했을 때에는
		1-1-1. "/" 또는 "\"가 있으면 슬라이싱 한 후 맨 뒤의 원소만 가져오기
	1-2. 파일명 형식이 유효한지 확인
		1-2-1. 입력한 파일명이 해당 폴더에 있는지 확인하는 코드. 없다면 예외 발생
2. 특수기호로 변환
3. 변환된 이름의 파일 copy

'''

import random, string, os, time

while 1:

	filename = input("\n파일명 입력(예: abc.txt), 끝내기는 exit를 입력: ")

	if filename == "exit":
		break

	elif not (filename in os.listdir()):
		print("\n해당 파일 없음. 재입력 바람")


	else:

		filename_list = filename.split(".")

		filename = filename_list[0]
		extension = filename_list[1]

		full_filename = filename + "." + extension

		special_characters = string.punctuation
		escape_characters = ['\\', '/', '*', ':', '?', '"', '<', '>', '|', '.', ';', '&', '=', '!', '`']

		for charater in escape_characters:
			special_characters = special_characters.replace(charater, "")

		new_filename = filename

		for character in new_filename:
			convert_character = random.choice(special_characters)
			new_filename = new_filename.replace(character, convert_character)

		new_full_filename = new_filename + "." + extension

		os.rename(full_filename, new_full_filename)

		print(f"{full_filename} => {new_full_filename}로 변환\n")

		time.sleep(5)

'''
oie 프로그램 오류로 인한 임시방편책
    - 발생 건만 추려 뽑기
'''

from oiescraper_sub import get_oie_report
from oiescraper_kor import translate_oie_reports
import re, pandas as pd, os, time, sqlite3, warnings
from tqdm import tqdm  # 진행표시줄

outbreak_urls = [
    'http://www.oie.int/wahis_2/public/wahid.php/Reviewreport/Review?page_refer=MapFullEventReport&reportid=35140',
    'http://www.oie.int/wahis_2/public/wahid.php/Reviewreport/Review?page_refer=MapFullEventReport&reportid=35169',
    'http://www.oie.int/wahis_2/public/wahid.php/Reviewreport/Review?page_refer=MapFullEventReport&reportid=35143',
    'http://www.oie.int/wahis_2/public/wahid.php/Reviewreport/Review?page_refer=MapFullEventReport&reportid=35149',
    'http://www.oie.int/wahis_2/public/wahid.php/Reviewreport/Review?page_refer=MapFullEventReport&reportid=35187',
    'http://www.oie.int/wahis_2/public/wahid.php/Reviewreport/Review?page_refer=MapFullEventReport&reportid=35173',
    'http://www.oie.int/wahis_2/public/wahid.php/Reviewreport/Review?page_refer=MapFullEventReport&reportid=35082',
    'http://www.oie.int/wahis_2/public/wahid.php/Reviewreport/Review?page_refer=MapFullEventReport&reportid=35162',
    'http://www.oie.int/wahis_2/public/wahid.php/Reviewreport/Review?page_refer=MapFullEventReport&reportid=35057',
    'http://www.oie.int/wahis_2/public/wahid.php/Reviewreport/Review?page_refer=MapFullEventReport&reportid=35163',
    'http://www.oie.int/wahis_2/public/wahid.php/Reviewreport/Review?page_refer=MapFullEventReport&reportid=35112',
    'http://www.oie.int/wahis_2/public/wahid.php/Reviewreport/Review?page_refer=MapFullEventReport&reportid=35114',
    'http://www.oie.int/wahis_2/public/wahid.php/Reviewreport/Review?page_refer=MapFullEventReport&reportid=35113',
    'http://www.oie.int/wahis_2/public/wahid.php/Reviewreport/Review?page_refer=MapFullEventReport&reportid=35193'
    ]


outbreak_linknumbers = [re.search('[0-9]{4,7}', url).group() for url in outbreak_urls]


def new_linknumber_data():
    
    reports_df = pd.DataFrame()

    for outbreak_linknumber in tqdm(outbreak_linknumbers):
    
        report = get_oie_report(outbreak_linknumber)[0]
        reports_df = reports_df.append(report, ignore_index=True)
        
        time.sleep(5)
        
    trans_dfs = translate_oie_reports(reports_df)
    
    
    # 엑셀 추출
    
    with pd.ExcelWriter('oie_reports.xlsx') as writer:
        trans_dfs[0].to_excel(writer, '원본', index=False)
        trans_dfs[1].to_excel(writer, '번역본', index=False)
    
    
    # db 저장
        
    if 'oie_reports.db' in os.listdir():  # os.listdir(path): path의 하위 폴더 및 파일 리스트. path 넣지 않으면 현재 경로로 설정
        con = sqlite3.connect('oie_reports.db')
    else:
        con = sqlite3.connect('./oiescraper/oie_reports.db')  # doc(.): 현재 경로
            
    warnings.filterwarnings(action='ignore')  # 경고 메시지 안 띄우게 함. action='ignore': 무시 'default': 경고 발생 위치에 출력
    
    trans_dfs[0].to_sql('oie_reports', con, if_exists='append', index=False)
    trans_dfs[1].to_sql('oie_reports_kr', con, if_exists='append', index=False)
    
    print('\n완료되었습니다.\n만들어진 엑셀 파일을 확인하세요.')  # 개수 입력 후 콘솔에 보여질 문구
    
    con.close()


if __name__ == "__main__":
    
    new_linknumber_data()
    
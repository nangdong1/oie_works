import os, time, requests, re, sqlite3, pandas as pd, warnings
from bs4 import BeautifulSoup
from tqdm import tqdm  # 진행표시줄
import urls, outbreak_urls
from oiescraper_sub import get_oie_report
from oiescraper_kor import translate_oie_reports

# '''뽑은 링크번호를 reports _url에 넣기'''

# url_list = []

# for new_linknumber in tqdm(new_linknumbers):  # tqdm: 반복문에서의 리스트 원소 수를 백분율로 나타내어 진행표시줄을 만듦
    
#     report_url = f'http://www.oie.int/wahis_2/public/wahid.php/Reviewreport/Review?page_refer=MapFullEventReport&reportid={new_linknumber}'
    
#     url_list.append(report_url)


previous_nums = urls.linknumbers

previous_new_nums = outbreak_urls.outbreak_linknumbers

extract_nums = []

for num in previous_nums:
    
    if not num in previous_new_nums:
        
        extract_nums.append(num)


reports_df = pd.DataFrame()

for extract_num in tqdm(extract_nums):

    report = get_oie_report(extract_nums)[0]
    reports_df = reports_df.append(report, ignore_index=True)
    
    time.sleep(5)
    

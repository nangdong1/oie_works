# -*- coding: utf-8 -*-

import pandas as pd, camelot, re

file = 'https://www.oie.int/wahis_2/temp/reports/en_fup_0000035856_20200924_103249.pdf'

tables = camelot.read_pdf(filepath=file, pages="1-end")

# tables.export('table_ex2.xlsx', f='xlsx', compress=True) 

outbreak_num_loc = tables[1].df.loc[5, 1]

outbreak_num = int(re.search('[0-9]+', outbreak_num_loc).group())

lat = []
lon = []

for n in range(2, 2+outbreak_num):
    
    df = tables[n].df
    
    text = df.loc[1, 6]
    
    if len(text.split()) < 2:
        
        continue
    
    lat.append(text.split()[0])
    lon.append(text.split()[1])
    

latlon_df = pd.DataFrame({"위도": lat, "경도": lon})    

latlon_df.to_excel("독일asf위경도.xlsx", index=False)
